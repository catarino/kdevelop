# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2013, 2014, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2015-08-05 20:15+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ghdialog.cpp:25
#, kde-format
msgid ""
"You have not authorized KDevelop to use your GitHub account. If you "
"authorize KDevelop, you will be able to fetch your public/private "
"repositories and the repositories from your organizations."
msgstr ""

#: ghdialog.cpp:34
#, kde-format
msgctxt "%1 is the URL with the GitHub token settings"
msgid "You can check the authorization for this application and others at %1"
msgstr ""

#: ghdialog.cpp:54
#, kde-format
msgid "You are logged in as <b>%1</b>.<br/>%2"
msgstr ""

#: ghdialog.cpp:58
#, fuzzy, kde-format
#| msgid "Log out"
msgctxt "@action:button"
msgid "Log Out"
msgstr "Kirjaudu ulos"

#: ghdialog.cpp:64
#, fuzzy, kde-format
#| msgid "Force Sync"
msgctxt "@action:button"
msgid "Force Sync"
msgstr "Pakota synkronointi"

#: ghdialog.cpp:78
#, fuzzy, kde-format
#| msgid "Authorize"
msgctxt "@action:button"
msgid "Authorize"
msgstr "Tunnistaudu"

#: ghdialog.cpp:92
#, fuzzy, kde-format
#| msgid "Github Account"
msgctxt "@title:window"
msgid "GitHub Account"
msgstr "Github-tili"

#: ghdialog.cpp:98
#, kde-format
msgid "Enter a login and a password"
msgstr "Kirjoita käyttäjätunnus ja salasana"

#: ghdialog.cpp:101 ghdialog.cpp:157 ghproviderwidget.cpp:44
#, kde-format
msgid "Waiting for response"
msgstr "Odotetaan vastausta"

#: ghdialog.cpp:126
#, fuzzy, kde-format
#| msgid "Authentication failed! Please, try again"
msgid ""
"Authentication failed. Please try again.\n"
"\n"
"Could not create token: \"%1\"\n"
"%2"
msgstr "Tunnistautuminen epäonnistui. Yritä uudelleen"

#: ghdialog.cpp:129
#, fuzzy, kde-format
#| msgid "Github Account"
msgctxt "@title:window"
msgid "GitHub Authorization Failed"
msgstr "Github-tili"

#: ghdialog.cpp:133
#, fuzzy, kde-format
#| msgid "Authentication failed! Please, try again"
msgid ""
"Authentication succeeded.\n"
"\n"
"Created token: \"%1\"\n"
"%2"
msgstr "Tunnistautuminen epäonnistui. Yritä uudelleen"

#: ghdialog.cpp:136
#, fuzzy, kde-format
#| msgid "Github Account"
msgctxt "@title:window"
msgid "GitHub Account Authorized"
msgstr "Github-tili"

#: ghdialog.cpp:144
#, kde-format
msgctxt "@title:window"
msgid "Authentication Code"
msgstr ""

#: ghdialog.cpp:144
#, kde-format
msgctxt "@label:textbox"
msgid "OTP Code:"
msgstr ""

#: ghdialog.cpp:176
#, kde-format
msgid "Please, write your password here."
msgstr "Kirjoita salasanasi tähän."

#: ghproviderplugin.cpp:35
#, kde-format
msgid "GitHub"
msgstr ""

#: ghproviderwidget.cpp:57
#, fuzzy, kde-format
#| msgid "Search"
msgctxt "@info:placeholder"
msgid "Search..."
msgstr "Etsi"

#: ghproviderwidget.cpp:58
#, fuzzy, kde-format
#| msgid "You can press the Return key if you don't want to wait"
msgctxt "@info:tooltip"
msgid "You can press the Return key if you do not want to wait."
msgstr "Voit painaa Enteriä, jos et halua odottaa"

#: ghproviderwidget.cpp:70
#, fuzzy, kde-format
#| msgid "Click this button to configure your Github account"
msgctxt "@info:tooltip"
msgid "Configure your GitHub account"
msgstr "Aseta Github-tilisi painamalla tästä"

#: ghproviderwidget.cpp:87
#, fuzzy, kde-format
msgid ""
"The Git plugin could not be loaded which is required to import a GitHub "
"project."
msgstr ""
"Github-projektin tuomiseen tarvittavan Git-liitännäisen lataaminen "
"epäonnistui."

#: ghproviderwidget.cpp:87
#, fuzzy, kde-format
msgctxt "@title:window"
msgid "GitHub Provider Error"
msgstr "Github-tarjoajan virhe"

#: ghproviderwidget.cpp:105
#, fuzzy, kde-format
#| msgid "User"
msgctxt "@item:inlistbox"
msgid "User"
msgstr "Käyttäjä"

#: ghproviderwidget.cpp:106
#, fuzzy, kde-format
#| msgid "Organization"
msgctxt "@item:inlistbox"
msgid "Organization"
msgstr "Organisaatio"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Lasse Liehu"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lasse.liehu@gmail.com"

#~ msgid "Github"
#~ msgstr "Github"

#~ msgid "Github Provider"
#~ msgstr "Github-tarjoaja"

#~ msgid "Import projects from Github"
#~ msgstr "Tuo projekteja Githubista"
